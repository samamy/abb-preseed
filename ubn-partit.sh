#!/usr/bin/env bash
echo "==> partitioning"
vgnm=vg00

sudo dd if=/dev/zero of=/dev/sdb bs=1k count=1                 
sudo blockdev --rereadpt /dev/sdb                              
sudo sh -c 'echo -e "g\nn\n\n\n\n\nw" | fdisk /dev/sdb'
sudo lsblk
sleep 1                                                   
sudo pvcreate /dev/sdb1                                        
sudo vgextend $vgnm /dev/sdb1                                   
sudo lvcreate -L2G -n lvswap $vgnm                              
sudo lvcreate -L7G -n lvvar $vgnm                            
sudo lvcreate -L7G -n lvusr $vgnm                             
sudo lvcreate -L3G -n lvopt $vgnm                              
sudo lvcreate -L5G -n lvhome $vgnm                              
sudo lvcreate -l100%FREE -n lvCUSTOM $vgnm 
sudo mkswap /dev/mapper/$vgnm-lvswap 
sudo mkfs.ext4 /dev/mapper/$vgnm-lvvar                          
sudo mkfs.ext4 /dev/mapper/$vgnm-lvusr                          
sudo mkfs.ext4 /dev/mapper/$vgnm-lvopt                          
sudo mkfs.ext4 /dev/mapper/$vgnm-lvhome                         
sudo mkfs.ext4 /dev/mapper/$vgnm-lvCUSTOM                      
sudo mkdir /tmp/var/ 
sudo mount /dev/mapper/$vgnm-lvvar /tmp/var/  
sudo mkdir /tmp/usr/ 
sudo mount /dev/mapper/$vgnm-lvusr /tmp/usr/  
sudo mkdir /tmp/home/ 
sudo mount /dev/mapper/$vgnm-lvhome /tmp/home/ 
sudo cp -a /var/* /tmp/var/                             
sudo cp -a /usr/* /tmp/usr/                             
sudo cp -a /home/* /tmp/home/                
sudo sed '/swapfile/d' /etc/fstab > /tmp/fstab
sudo mv /tmp/fstab /etc/fstab
sudo swapoff --all     
sudo rm /swapfile 
mkdir /CUSTOM                             
sudo sh -c 'echo "/dev/mapper/vg00-lvswap none swap sw 0 0\n/dev/mapper/vg00-lvvar /var ext4 defaults 0 0\n/dev/mapper/vg00-lvusr /usr ext4 defaults 0 0\n/dev/mapper/vg00-lvopt /opt ext4 defaults 0 0\n/dev/mapper/vg00-lvhome /home ext4 defaults 0 0\n/dev/mapper/vg00-lvCUSTOM /CUSTOM ext4 defaults 0 0" >> /etc/fstab'
cat /etc/fstab
sudo swapon --all
sudo umount /tmp/var                                          
sudo umount /tmp/usr                                          
sudo umount /tmp/home                                        
sleep 1 
sudo rm -rf /tmp/var
sudo rm -rf /tmp/usr
sudo rm -rf /tmp/home
sudo mount /var/
sudo mount /usr/
sudo mount /opt/
sudo mount /home/
echo "==> clean old /usr/ /opt/ /home/"
sudo mount --bind / /mnt
sudo rm -rf /mnt/var/*
sudo rm -rf /mnt/usr/*
sudo rm -rf /mnt/home/*
sudo umount /mnt

echo "==> setup users"
echo "==> add ansibleuser user"
sudo sh -c 'echo "ansibleuser ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansibleuser'
sudo chmod 440 /etc/sudoers.d/ansibleuser
sudo useradd -G sudo -s /bin/bash ansibleuser
sudo mkdir /home/ansibleuser/
sudo mkdir /home/ansibleuser/.ssh/
sudo sh -c 'echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCY2rE75iCstyLsKhdH3Tm0nTR8ejGURkoeJ89hRMEQkq5cuuWZwIS0isf2b1w0PBL85ROj/cN/UODLW1q/jc+IrWEFdpRpmwwj2LotXDnDyvoV2gaki+0VhyOMzxsdSSmmN/szNFxXV+MsatE2jD4FcD8wXSkRgqeYpzPP8TJlA7lweUh1DPYmJLanP3h6oPh9uRuIg8vHDY8DSvAHwvQVT7SwuIwuZ4IiDG2at1WPSnINxcUm0J6+9zQgSrK39sCo4esaP+ioIXKIfYx9RC8G0Q6gIPWh8bHRWClshuig9rnRnktEUGJDxa1kE6xG7vYTqOeEig6jVW+d/HGxVe3x" > /home/ansibleuser/.ssh/authorized_keys'
sudo chmod 644 /home/ansibleuser/.ssh/authorized_keys
sudo chown -R ansibleuser:ansibleuser /home/ansibleuser/

echo "==> add admin user"
sudo sh -c 'echo "admin ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/admin'
sudo chmod 440 /etc/sudoers.d/admin
sudo useradd -G sudo -s /bin/bash admin
#echo -e 'password\npassword' | sudo passwd admin ### setup password
sudo mkdir /home/admin/
sudo mkdir /home/admin/.ssh/
sudo sh -c 'echo "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAgYEAIKqiqZbzV/CHPrDPLpXo53sEkDOH9DhG5k7jR9HM7KxedLeojPwRsdTVPMpvA5bxR9ANC0rHee1z8bB1DY5Vacx+As4rFI7ohbxP1rNbd9TWkwpQCLItN0kqOUwGmYU3O9D6MhaJ1/CVUqARDSQW9tvIb1iYhuX4UWf3ds6Xa4vGj+/zfOHz3g/2AdTBrx5Lfd3zS41yBJkAe6HmNUGuLx79Bw66BXiio5SmFHf0nFwkQ201r9nNYzf+5qZrY4ee3CZQw3QjPUh6Z+pWBo5K6S87HbXDRvI2MrTj2fbOslR5bATuQHaHAaVum4ozfitGOFmc5YFahWo2nXzrQQ==" > /home/admin/.ssh/authorized_keys'
sudo chmod 644 /home/admin/.ssh/authorized_keys
sudo chown -R admin:admin /home/admin/

echo "==> setup sshd"
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.backup
sudo sed -e 's/^#PermitEmptyPassword.*/PermitEmptyPasswords no/' \
  -e 's/^#PermitRootLogin.*/PermitRootLogin no/' \
  -e 's/^#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/' \
  -e 's/^#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config > /tmp/sshd_config
sudo mv /tmp/sshd_config /etc/ssh/sshd_config

echo "==> setup sysctl"
sudo echo -e 'net.ipv6.conf.all.disable_ipv6=1\nnet.ipv6.conf.default.disable_ipv6=1\nnet.ipv6.conf.lo.disable_ipv6=1\nnet.ipv4.ip_forward=0\nnet.ipv6.conf.all.forwarding=0\nvm.swappiness=5' >> /etc/sysctl.conf
sudo sysctl -p
