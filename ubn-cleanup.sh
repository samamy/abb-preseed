#!/usr/bin/env bash

echo "==> stop loggign service"

sudo service rsyslog stop

echo "==> clean logs"

if [ -f /var/log/wtmp ]; then
    sudo truncate -s0 /var/log/wtmp
fi
if [ -f /var/log/lastlog ]; then
    sudo truncate -s0 /var/log/lastlog
fi

if [ -f /var/log/auth.log ]; then
    truncate -s0 /var/log/auth.log
fi

if [ -f /var/log/alternatives.log ]; then
    truncate -s0 /var/log/alternatives.log
fi

echo "==> clean temp files"

sudo rm -rf /tmp/*
sudo rm -rf /var/tmp/*

echo "==> clean machine-id"

sudo /bin/rm -rf /etc/machine-id

echo "==> create new machine-id"

#sudo systemd-machine-id-setup
sudo bash -c 'echo -n > /etc/machine-id'

echo "==> clear previous network configuration"

sudo rm -rf /etc/netplan/01-netcfg.yaml

sudo bash -c 'cat >>/etc/netplan/01-netcfg.yaml<< EOF
network:
  version: 2
  renderer: networkd
  ethernets:
    ens33:
      dhcp4: true
EOF'

echo "==> new dhcp configuration apply with next boot"

echo " ==> clear shell history"

history -c

sudo touch /etc/rc.local
sudo chmod +x /etc/rc.local
sudo sh -c 'echo "#!/bin/sh -e\n/usr/sbin/deluser --remove-home abbuser\n/bin/rm /etc/rc.local\nexit 0" > /etc/rc.local'
sudo systemctl enable rc-local
